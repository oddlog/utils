import {escape} from "./fast-string";
import {assign, assignAll, assignWeak, clone, cloneDeep, identity, once, self, stubFalse, stubTrue} from "./lodash";
import {callerLocation} from "./source-location";

/*===================================================== Exports  =====================================================*/

export {

  escape,

  callerLocation,

  assign, assignAll, assignWeak, clone, cloneDeep, identity, once, self, stubFalse, stubTrue,

};
