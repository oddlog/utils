export {callerLocation};

/*==================================================== Functions  ====================================================*/

function callerLocation(deepness) {
  const str = new Error().stack.split("\n", deepness + 1)[deepness];
  if (str == null) { return null; }
  const locationString = str.substring(str.lastIndexOf("(") + 1, str.lastIndexOf(")"));
  const split = locationString.split(":");
  if (split.length !== 3) { return null; }
  return {
    file: split[0],
    row: +split[1],
    col: +split[2]
  };
}
