export {escape};

/*==================================================== Functions  ====================================================*/

/**
 * Creates an escaped version of the passed string. Char codes below 32 (non-printed chars) are stripped.
 *
 * @param {String} str The input string.
 * @returns {String} The escaped string.
 */
function escape(str) {
  let result = "";
  let someEscape = false;
  let lastIdx = 0;
  const _len = str.length;
  for (let i = 0; i < _len; i++) {
    const code = str.charCodeAt(i);
    if (code === 0x22 /* DOUBLE_QUOTE */ || code === 0x5C /* BACKSLASH */) {
      result += str.slice(lastIdx, i) + "\\";
      lastIdx = i;
      someEscape = true;
    } else if (code < 32) {
      result += str.slice(lastIdx, i);
      lastIdx = i + 1;
    }
  }
  return someEscape ? result + str.slice(lastIdx) : str;
}
