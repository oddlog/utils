export {
  // function operations
  identity, self, once, stubTrue, stubFalse,
  // object/array operations
  assign, assignAll, assignWeak, clone, cloneDeep
};

/*==================================================== Functions  ====================================================*/

/*----------------------------------------------- function operations  -----------------------------------------------*/

// return methods

function identity(value) { return value; }

function self() { return this; } // eslint-disable-line no-invalid-this

function stubTrue() { return true; }

function stubFalse() { return false; }

// function guarding

function once(cb) {
  let called = false;
  return function (...args) {
    if (!called) {
      called = true;
      Reflect.apply(cb, null, args);
    }
  };
}

/*------------------------------------------------ object operations  ------------------------------------------------*/

/**
 * Creates a shallow copy of the passed data. If it's neither an object nor an array, it is returned itself.
 *
 * @param {?Object|Array|*} obj The data to clone.
 * @returns {?Object|Array|*} The cloned data.
 */
function clone(obj) {
  if (obj == null || typeof obj !== "object") { return obj; }
  if (Array.isArray(obj)) {
    if (obj.length > 16) { return obj.slice(); } // see benchmarks
    const arr = [], _len = obj.length;
    for (let i = 0; i < _len; i++) { arr.push(obj[i]); }
    return arr;
  } else {
    const clone = {};
    for (let key in obj) { if (obj.hasOwnProperty(key)) { clone[key] = obj[key]; } }
    return clone;
  }
}

/**
 * Creates a deep copy of the passed data. Properties that are neither an object nor an array, are assigned themselves.
 *
 * @param {?Object|Array|*} obj The data to clone.
 * @returns {?Object|Array|*} The cloned data.
 */
function cloneDeep(obj) {
  if (obj == null || typeof obj !== "object") { return obj; }
  if (Array.isArray(obj)) {
    let arr = [], _len = obj.length, i;
    for (i = 0; i < _len; i++) { arr.push(cloneDeep(obj[i])); }
    return arr;
  } else {
    let clone = {}, key;
    for (key in obj) { if (obj.hasOwnProperty(key)) { clone[key] = cloneDeep(obj[key]); } }
    return clone;
  }
}

function assign(target, source) {
  if (typeof target !== "object") { return target; }
  for (let key in source) { if (source.hasOwnProperty(key)) { target[key] = source[key]; } }
  return target;
}


function assignAll(target, ...sources) {
  if (typeof target !== "object") { return target; }
  for (let source of sources) {
    for (let key in source) { if (source.hasOwnProperty(key)) { target[key] = source[key]; } }
  }
  return target;
}

function assignWeak(target, source) {
  if (typeof target !== "object") { return target; }
  for (let key in source) {
    if (source.hasOwnProperty(key) && !target.hasOwnProperty(key)) { target[key] = source[key]; }
  }
  return target;
}
